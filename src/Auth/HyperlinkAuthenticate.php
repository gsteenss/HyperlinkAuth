<?php
namespace Muffin\HyperlinkAuth\Auth;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Auth\FormAuthenticate;
use Cake\Controller\ComponentRegistry;
use Cake\Http\ServerRequest;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;

class HyperlinkAuthenticate extends FormAuthenticate
{
    protected $_defaultConfig = [
        'token' => [
            'parameter' => 'token',
            'detector' => 'token',
            'length' => 10,
            'expires' => '+10 mins',
            'finder' => null,
            'factory' => null,
        ],
        'fields' => [
            'username' => 'email',
            'token' => 'token',
            'expires' => 'token_expiry',
        ],
        'userModel' => 'Users',
        'scope' => [],
        'finder' => 'all',
        'contain' => null,
    ];

    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);

        ServerRequest::addDetector($this->getConfig('token.detector'), function(ServerRequest $request) {
            return (bool)$request->query($this->getConfig('token.parameter'))
                || (bool)$request->param($this->getConfig('token.parameter'));
        });

        if (!$this->getConfig('token.factory')) {
            $this->setConfig('token.factory', [$this, '_tokenize']);
        }
    }

    public function authenticate(ServerRequest $request, Response $response)
    {
        $config = $this->getConfig();

	// prevent error when user is not found in database
	$this->_passwordHasher = new DefaultPasswordHasher();
	
        if (!$request->is($config['token']['detector'])) {
            return $this->_findUser($request->data[$this->getConfig('fields.username')]);
        }

        $token = $request->param($config['token']['parameter']);
        if (!$token) {
            $token = $request->query($config['token']['parameter']);
        }

        if ($finder = $this->getConfig('token.finder')) {
            return call_user_func($finder, $token);
        }

        $this->setConfig('fields.username', $this->getConfig('fields.token'));
        return $this->_findUser($token);
    }

    public function token(array $user)
    {
	Log::debug($user);
	if (!empty($user)) 
	{ 
	  $current_token=$user[$this->getConfig('fields.token')];
	  $now=FrozenTime::now();
	  Log::debug($now);
	  // if current token is still valid & unused, return it instead of generating a new one:
	  if (!empty($current_token) && $user[$this->getConfig('fields.expires')]->gte($now)) return $current_token;	    	    
	}
	// if not: generate a new token 
        return call_user_func($this->getConfig('token.factory'), $user);
    }

    protected function _tokenize(array $user)
    {
        $config = $this->_config;
        $fields = $config['fields'];
        $table = TableRegistry::getTableLocator()->get($config['userModel']);
        $conditions = [$fields['username'] => $user[$fields['username']]];
        $data = [
            $fields['token'] => base64_encode(Security::randomBytes($config['token']['length'])),
            $fields['expires'] => (new \DateTime($config['token']['expires']))->format('Y-m-d H:i:s'),
        ];
        $table->updateAll($data, $conditions);
        return $data[$fields['token']];
    }
}
